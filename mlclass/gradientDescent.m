function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)

m = length(y);
J_history = zeros(num_iters, 1);
dJ=zeros(2,1);
for iter = 1:num_iters

   

dJ(1)=sum((X*theta-y).*X(:,1));
dJ(2)=sum((X*theta-y).*X(:,2));

dJ=dJ*alpha/m;


theta=theta-dJ;

J_history(iter) = computeCost(X, y, theta);

end

end
